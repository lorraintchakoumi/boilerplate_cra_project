import { useRoutes } from "react-router";
import { routes } from "./routes/index";
import theme from "./theme/index";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, Flip } from "react-toastify";
import { ThemeProvider } from "@mui/material";

function App() {
  const routing = useRoutes(routes);
  return (
    <ThemeProvider theme={theme}>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        transition={Flip}
      />
      {routing}
    </ThemeProvider>
  );
}

export default App;
