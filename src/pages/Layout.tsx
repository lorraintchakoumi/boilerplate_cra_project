import { Box, Typography } from "@mui/material";
import { Outlet } from "react-router";
import theme from "../theme";

export default function Layout() {
  return (
    <Box
      sx={{
        display: "grid",
        gridTemplateRows: "auto 1fr",
        minHeight: "100vh",
        rowGap: theme.spacing(2),
      }}
    >
      <Typography
        sx={{
          backgroundColor: theme.palette.primary.light,
          color: "white",
          fontWeight: 700,
          fontSize: "1.5rem",
          padding: "10px",
        }}
      >
        Welcome to Interview App
      </Typography>
      <Outlet />
    </Box>
  );
}
