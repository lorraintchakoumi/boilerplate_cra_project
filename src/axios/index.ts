import axios from 'axios';

export const http = axios.create({
  baseURL: 'https://imbd8.p.rapidapi.com',
  headers: {
    'Content-type': 'application/json',
    'X-RapidAPI-Key': '082a88fa72mshaf5e7dc82b8ea42p1ceb8fjsne2a7b006a34f',
    'X-RapidAPI-Host': 'imdb8.p.rapidapi.com',
  },
  //   withCredentials: true,
});

export default http