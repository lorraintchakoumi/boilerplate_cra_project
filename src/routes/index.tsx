import { Navigate } from "react-router";
import Layout from "../pages/Layout";
import TestPage from "../pages/Test";

export const routes = [
  {
    path: "/",
    element: <Layout />,
    children: [{ path: "/", element: <TestPage /> }],
  },
  { path: "*", element: <Navigate to="/" /> },
];
