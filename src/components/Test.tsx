import { useEffect, useState } from "react";
import { ReportRounded } from "@mui/icons-material";
import useNotification, {
  ErrorMessage,
  filterNotificationUsage,
  NotificationInterface,
} from "../notification";
import { loadIMBD } from "../services/IMBD";

export default function Test() {
  const [isDataLoading, setIsDataLoading] = useState<boolean>(false);
  const [notifications, setNotifications] = useState<NotificationInterface[]>(
    []
  );

  const loadIMBDData = () => {
    setIsDataLoading(true);
    const notif = new useNotification();
    setNotifications(
      filterNotificationUsage("data_loading", notif, notifications)
    );
    notif.notify({ render: "Please wait while we load IMBD data" });

    loadIMBD()
      .then(({ d }) => {
        notif.update({ render: "Data loaded successfully. Happy play around" });
        console.log(d);
      })
      .catch((e) => {
        notif.update({
          type: "ERROR",
          render: (
            <ErrorMessage
              retryFunction={loadIMBDData}
              notification={notif}
              message={
                "Something went wrong while loading IMBD Data. Please try again"
              }
            />
          ),
          autoClose: false,
          icon: () => <ReportRounded fontSize="medium" color="error" />,
        });
        console.log(e);
      })
      .finally(() => setIsDataLoading(false));
  };
  useEffect(() => {
    loadIMBDData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return <div>{isDataLoading ? "Loading Test" : "Test"}</div>;
}
